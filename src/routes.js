import MainTable from './components/MainTable.vue'
import FileUpload from './components/FileUpload.vue'

export const routes = [
    {path: '/upload', name: 'upload', component: FileUpload},
    {path: '/table', name: 'table', component: MainTable},
    {path: '/', name: 'table', component: MainTable},
]